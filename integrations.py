#%% [markdown]
# # <center>Laboratory 9<br>Computing methods</center>
#%% [markdown]
# **Instruction:**  
# All the exercises from the laboratory must be implemented in [Spyder](https://www.spyder-ide.org/). The report must be sent in [Jupyter Notebook](https://jupyter-notebook.readthedocs.io/en/stable/)  
# 
#%% [markdown]
# ***Aim of the lab:***  
# The aim of the course is to familiarize yourself with the methods of numerical integration of functions:
# $\int\limits_{a}^{b}f(x) $  
# 
#%% [markdown]
# ***Exercise 1.***  
# Calculate numerically:  
# $\int\limits_{a}^{b}\log(1+\tan(x)) $      
# 
# Do the following:
# 1. Is it possible to integrate the given function on any interval? Give reasons for your answer.
# 2. Calculate the integral numerically in the range $a =0$, $b=\cfrac{\pi}{4}$ by the following methods:
#     1. Trapeze method - [trapz](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.trapz.html#scipy.integrate.trapz)
#     2. The Simpson method - [simps](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.simps.html#scipy.integrate.simps)
#     3. Romberg's method - [romb](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.romb.html#scipy.integrate.romb)
#   
# Check how the results differ from each other, whether they are affected by the length of the $ \mathbf{y} $ vector and the value of the $ dx $ parameter
# 3. Compare the results and calculation time from point 1 with the functions:
#     1. [quad](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.quad.html#scipy-integrate-quad)
#     2. [romberg](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.romberg.html#scipy.integrate.romberg)
#     3. [quadrature](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.quadrature.html#scipy.integrate.quadrature)
#%% [markdown]
# ***Exercise 2.***  
# The driver driving from city A to city B, noticing an obstacle on the road, began to brake rapidly until he stopped after four seconds. Acceleration of his car registered by an accidental physicist observing the event is shown in the table below.  
# 
# |time \[s\]|acceleration \[$\frac{m}{s^2}$\]|
# |--|--|
# |0.0|-4.5|
# |0.5|-4.5|
# |1.0|-2.3|
# |2.0|-1.0|
# |3.0|-0.5|
# |4.0|0.0|
# 
# * Calculate the braking distance of the vehicle.
# * Calculate the average speed of the car in the range $t=\left[0,0.5\right]$
#%% [markdown]
# Supplementary materials:
# - [Scipy Lecture Notes](http://www.scipy-lectures.org/index.html)
# - [NumPy for Matlab users](https://docs.scipy.org/doc/numpy/user/numpy-for-matlab-users.html#numpy-for-matlab-users)
# - [Python Tutorial - W3Schools](https://www.w3schools.com/python/default.asp)
# - [NumPy](https://www.numpy.org)
# - [Matplotlib](https://matplotlib.org/)
# - [Anaconda](https://www.anaconda.com/)
# - [Learn Python for Data Science](https://www.datacamp.com/learn-python-with-anaconda?utm_source=Anaconda_download&utm_campaign=datacamp_training&utm_medium=banner)
# - [Learn Python](https://www.learnpython.org/)
# - [Uncle  Google](https://google.pl) i [auntie Wikipedia](https://pl.wikipedia.org/wiki/Wikipedia:Strona_g%C5%82%C3%B3wna)

#%%
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import trapz, simps, romb, quad, romberg, quadrature
#trapz
a = 0
b = np.pi/4
x, step = np.linspace(a, b, 129, retstep=True)
def f(x):
    return np.log(1+np.tan(x))
plt.plot(x, f(x))
#%%
import time

time_start = time.clock()
sum_trapz = trapz(f(x), x)
time_elapsed_trapz = (time.clock() - time_start)

time_start = time.clock()
sum_simps = simps(f(x), x)
time_elapsed_trapz = (time.clock() - time_start)

time_start = time.clock()
sum_romb = romb(f(x), step)
time_elapsed_romb = (time.clock() - time_start)

print('integral value for trapz: {}, simpson: {}, romberg: {} method'.format(sum_trapz, sum_simps, sum_romb))
#%%
sum_quad, err = quad(f, a, b)
sum_romberg = romberg(f, a, b)
sum_quadrature = quadrature(f, a, b)
print('integral value for quad: {}, romberg: {}, quadrature: {} method'.format(sum_quad, sum_romberg, sum_quadrature))

#%%
time = np.array([0.0, 0.5, 1.0, 2.0, 3.0, 4.0])
acceleration = np.array([-4.5, -4.5, -2.3, -1.0, -0.5, 0.0])
plt.plot(time, acceleration, 'o')
plt.xlabel('time[s]')
plt.ylabel('acceleration[m/s^2]')
#%% [markdown]
# The equation for velocity for given acceleration can be described as below
# $$ V(t_k) = \int_{t_0}^{t_k} a(t) dt + V(t_0) $$
# where $V(t_k)$ (the last value of velocity - k = 4s) is equal to 0 (the driver stopped after 4 seconds), so the initial velociy is equal to 
# $$ V(t_0) = - \int_{t_0}^{t_k} a(t) dt $$
# %%
initial_velocity = -trapz(acceleration, time)
print(initial_velocity)
# %% [markdown]
# Before I can get the total braking distance I need the value of velocity in each point. 
# For time $t_i$ the velocity is equal to 
# $$ V(t_i) = \int_{t_0}^{t_i} a(t) dt + V(t_0) $$
# %%
velocity_vect = [trapz(acceleration[0:i+2], time[0:i+2]) for i in range(len(time)-1)] + initial_velocity
velocity = np.insert(velocity_vect, 0, initial_velocity)
plt.plot(time, velocity, 'o')
plt.xlabel('time[s]')
plt.ylabel('velocity[m/s]')

# %% [markdown]
# Now I can calculate the braking distance 
# $$ \Delta x = x(t_k) - x(t_0) = \int_{t_0}^{t_k} v(t) dt $$
# %%
braking_distance = trapz(velocity, time) 
print(braking_distance)
# %% [markdown]
# To calculate the average velocity in the first 0.5s I need to divide the total displacement by time (0.5s):
# $$ \bar{V}(t_{0 -> 0.5}) = {{\int_{0}^{0.5} v(t) dt} \over {0.5}}$$
# %%
braking_distance_0_5 = trapz(velocity[:2], time[:2])
average_velocity_0_5 = braking_distance_0_5/time[1]
print(average_velocity_0_5)
#%%
